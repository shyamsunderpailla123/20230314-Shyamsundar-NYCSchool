//
//  SchoolsListViewModel.swift
//  20230314-ShyamSundar-NYCSchools
//
//  Created by ShyamSundar on 3/15/23.
//

import Foundation

class SchoolsListViewModel {
    var schoolsData: SchoolsList?

    func getSchoolsList(completion: @escaping (Bool) -> ()) {
        NetworkLayer().getSchoolsList(endPoint: ApiUrls.BaseUrl.cityOfNewyork + ApiUrls.EndPoints.schoolsList) { (newsData, isSuccess) in
            if isSuccess {
                self.schoolsData = newsData
                completion(isSuccess)
            } else {
                completion(false)
            }
        }
    }
}
