//
//  SchoolsListModel.swift
//  20230314-ShyamSundar-NYCSchools
//
//  Created by ShyamSundar on 3/15/23.
//

import Foundation

struct School: Codable {
    let dbn, schoolName, phoneNumber, website: String

    enum CodingKeys: String, CodingKey {
        case dbn
        case website
        case schoolName = "school_name"
        case phoneNumber = "phone_number"
    }
}


struct SchoolsList: Codable {
    var schools: [School]
}
