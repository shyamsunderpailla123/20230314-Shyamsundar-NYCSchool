//
//  String+Extensions.swift
//  20230314-ShyamSundar-NYCSchools
//
//  Created by ShyamSundar on 15/03/23.
//

import Foundation

extension String {
    
    func localized(bundle: Bundle = .main, tableName: String = "Localizable", comment: String = "") -> String {
        return NSLocalizedString(self, tableName: tableName, bundle: bundle, comment: comment)
    }
    
}
