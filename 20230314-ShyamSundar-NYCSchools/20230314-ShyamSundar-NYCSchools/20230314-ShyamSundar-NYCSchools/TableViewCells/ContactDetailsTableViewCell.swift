//
//  ContactDetailsTableViewCell.swift
//  20230314-ShyamSundar-NYCSchools
//
//  Created by ShyamSundar on 3/15/23.
//

import UIKit

class ContactDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setschoolContactDetails(school: School) {
        schoolNameLabel.text = school.schoolName
        emailLabel.text = "Website: " + school.website
        phoneNumberLabel.text = "PhoneNumber: " + school.phoneNumber
    }
}
